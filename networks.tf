#Get all available AZ's in VPC for master region
data "aws_availability_zones" "azs" {
  provider = aws.region-master
  state    = "available"
}
data "aws_availability_zones" "azs_uswest2" {
  provider = aws.region-worker
  state    = "available"
}
#___________JENKINS MASTER NETWORK_____________________________________________________________
#Create VPC in us-east-1
resource "aws_vpc" "vpc_master" {
  provider             = aws.region-master
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "master-vpc-jenkins"
  }
}
#Create IGW in us-east-1
resource "aws_internet_gateway" "igw" {
  provider = aws.region-master
  vpc_id   = aws_vpc.vpc_master.id
}
#Create subnet # 1 in us-east-1
resource "aws_subnet" "subnet_1" {
  provider          = aws.region-master
  availability_zone = element(data.aws_availability_zones.azs.names, 0)
  vpc_id            = aws_vpc.vpc_master.id
  cidr_block        = "10.0.1.0/24"
}
#Create subnet #2  in us-east-1
resource "aws_subnet" "subnet_2" {
  provider          = aws.region-master
  vpc_id            = aws_vpc.vpc_master.id
  availability_zone = element(data.aws_availability_zones.azs.names, 1)
  cidr_block        = "10.0.2.0/24"
}
#Create route table in us-east-1
resource "aws_route_table" "internet_route" {
  provider = aws.region-master
  vpc_id   = aws_vpc.vpc_master.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  route {
    cidr_block                = "192.168.1.0/24"
    vpc_peering_connection_id = aws_vpc_peering_connection.useast1-uswest2.id
  }
  lifecycle {
    ignore_changes = all
  }
  tags = {
    Name = "Master-Region-RT"
  }
}
#Overwrite default route table of VPC(Master) with our route table entries
resource "aws_main_route_table_association" "set-master-default-rt-assoc" {
  provider       = aws.region-master
  vpc_id         = aws_vpc.vpc_master.id
  route_table_id = aws_route_table.internet_route.id
}
#___________JENKINS WORKER NETWORK__________________________________________________________________________
#Create VPC in us-west-2
resource "aws_vpc" "vpc_master_oregon" {
  provider             = aws.region-worker
  cidr_block           = "192.168.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "worker-vpc-jenkins"
  }

}
#Create IGW in us-west-2
resource "aws_internet_gateway" "igw-oregon" {
  provider = aws.region-worker
  vpc_id   = aws_vpc.vpc_master_oregon.id
}
#Create subnet in us-west-2
resource "aws_subnet" "subnet_1_oregon" {
  provider   = aws.region-worker
  vpc_id     = aws_vpc.vpc_master_oregon.id
  cidr_block = "192.168.1.0/24"
}
#Create route table in us-west-2
resource "aws_route_table" "internet_route_oregon" {
  provider = aws.region-worker
  vpc_id   = aws_vpc.vpc_master_oregon.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw-oregon.id
  }
  route {
    cidr_block                = "10.0.1.0/24"
    vpc_peering_connection_id = aws_vpc_peering_connection.useast1-uswest2.id
  }
  lifecycle {
    ignore_changes = all
  }
  tags = {
    Name = "Worker-Region-RT"
  }
}
#Overwrite default route table of VPC(Worker) with our route table entries
resource "aws_main_route_table_association" "set-worker-default-rt-assoc" {
  provider       = aws.region-worker
  vpc_id         = aws_vpc.vpc_master_oregon.id
  route_table_id = aws_route_table.internet_route_oregon.id
}
#_________________________________________NEXUS  NETWORK_____________________________________________________
resource "aws_vpc" "vpc_nexus" {
  provider             = aws.region-master
  cidr_block           = "192.168.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "nexus-vpc"
  }

}
#Create IGW in us-east-1
resource "aws_internet_gateway" "igw-nexus" {
  provider = aws.region-master
  vpc_id   = aws_vpc.vpc_nexus.id
}
#Create subnet #2  in us-east-1
resource "aws_subnet" "subnet_nexus_1" {
  provider          = aws.region-master
  vpc_id            = aws_vpc.vpc_nexus.id
  availability_zone = element(data.aws_availability_zones.azs.names, 0)
  cidr_block        = "192.168.1.0/24"
}
resource "aws_subnet" "subnet_nexus_2" {
  provider          = aws.region-master
  vpc_id            = aws_vpc.vpc_nexus.id
  availability_zone = element(data.aws_availability_zones.azs.names, 1)
  cidr_block        = "192.168.2.0/24"
}
#Create route table in us-east-1
resource "aws_route_table" "internet_route_nexus" {
  provider = aws.region-master
  vpc_id   = aws_vpc.vpc_nexus.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw-nexus.id
  }

  lifecycle {
    ignore_changes = all
  }
  tags = {
    Name = "Nexus-RT"
  }
}
#Overwrite default route table of VPC(Master) with our route table entries
resource "aws_main_route_table_association" "set-nexus-default-rt-assoc" {
  provider       = aws.region-master
  vpc_id         = aws_vpc.vpc_nexus.id
  route_table_id = aws_route_table.internet_route_nexus.id
}
#__________________________________________SONARQUBE NETWORK________________________________________________
resource "aws_vpc" "vpc_sonar" {
  provider             = aws.region-worker
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "sonar-vpc"
  }

}
#Create IGW in us-east-1
resource "aws_internet_gateway" "igw-sonar" {
  provider = aws.region-worker
  vpc_id   = aws_vpc.vpc_sonar.id
}
#Create subnet #2  in us-east-1
resource "aws_subnet" "subnet_sonar_1" {
  provider          = aws.region-worker
  vpc_id            = aws_vpc.vpc_sonar.id
  availability_zone = element(data.aws_availability_zones.azs_uswest2.names, 0)
  cidr_block        = "10.0.1.0/24"
}
resource "aws_subnet" "subnet_sonar_2" {
  provider          = aws.region-worker
  vpc_id            = aws_vpc.vpc_sonar.id
  availability_zone = element(data.aws_availability_zones.azs_uswest2.names, 1)
  cidr_block        = "10.0.2.0/24"
}
#Create route table in us-east-1
resource "aws_route_table" "internet_route_sonar" {
  provider = aws.region-worker
  vpc_id   = aws_vpc.vpc_sonar.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw-sonar.id
  }

  lifecycle {
    ignore_changes = all
  }
  tags = {
    Name = "Sonarqube-RT"
  }
}
#Overwrite default route table of VPC(Master) with our route table entries
resource "aws_main_route_table_association" "set-sonar-default-rt-assoc" {
  provider       = aws.region-worker
  vpc_id         = aws_vpc.vpc_sonar.id
  route_table_id = aws_route_table.internet_route_sonar.id
}
#___________________________________________________________________________________________________________

#Initiate Peering connection request from us-east-1
resource "aws_vpc_peering_connection" "useast1-uswest2" {
  provider    = aws.region-master
  peer_vpc_id = aws_vpc.vpc_master_oregon.id
  vpc_id      = aws_vpc.vpc_master.id
  peer_region = var.region-worker

}

#Accept VPC peering request in us-west-2 from us-east-1
resource "aws_vpc_peering_connection_accepter" "accept_peering" {
  provider                  = aws.region-worker
  vpc_peering_connection_id = aws_vpc_peering_connection.useast1-uswest2.id
  auto_accept               = true
}
# #________________________________________________________________________________________________________________
# #Initiate Peering connection request from us-east-1
# resource "aws_vpc_peering_connection" "jenkins-sonar" {
#   provider    = aws.region-master
#   peer_vpc_id = aws_vpc.vpc_sonar.id
#   vpc_id      = aws_vpc.vpc_master.id
#   peer_region = var.region-worker

# }
# #Accept VPC peering request in us-west-2 from us-east-1
# resource "aws_vpc_peering_connection_accepter" "accept_peering_sonar" {
#   provider                  = aws.region-worker
#   vpc_peering_connection_id = aws_vpc_peering_connection.jenkins-sonar.id
#   auto_accept               = true
# }
# #________________________________________________________________________________________________________________
# #Initiate Peering connection request from us-east-1
# resource "aws_vpc_peering_connection" "jenkins-nexus" {
#   provider    = aws.region-master
#   peer_vpc_id = aws_vpc.vpc_nexus.id
#   vpc_id      = aws_vpc.vpc_master.id
#   peer_region = var.region-master

# }
# #Accept VPC peering request in us-west-2 from us-east-1
# resource "aws_vpc_peering_connection_accepter" "accept_peering_nexus" {
#   provider                  = aws.region-master
#   vpc_peering_connection_id = aws_vpc_peering_connection.jenkins-nexus.id
#   auto_accept               = true
# }



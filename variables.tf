variable "profile" {
  type    = string
  default = "default"
}

variable "region-master" {
  type    = string
  default = "us-east-1"
}

variable "region-worker" {
  type    = string
  default = "us-west-2"
}
variable "external_ip" {
  type    = string
  default = "0.0.0.0/0"
}
variable "workers-count" {
  type    = number
  default = 3
}

variable "instance-type" {
  type    = string
  default = "t2.medium"
}
variable "webserver-port" {
  type    = number
  default = 8080
}
variable "nexus-webserver-port" {
  type    = number
  default = 8081
}
variable "sonar-webserver-port" {
  type    = number
  default = 9000
}
variable "dns-name" {
  type    = string
  default = "cmcloudlab1612.info."
}

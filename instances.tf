#Get Linux AMI ID using SSM Parameter endpoint in us-east-1
# data "aws_ssm_parameter" "linuxAmi" {
#   provider = aws.region-master
#  # name     = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
#   name   = "ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"

# }
data "aws_ami" "ubuntu_master" {
  provider    = aws.region-master
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

#Get Linux AMI ID using SSM Parameter endpoint in us-west-2
# data "aws_ssm_parameter" "linuxAmiOregon" {
#   provider = aws.region-worker
#   name     = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
# }
data "aws_ami" "ubuntu_worker" {
  provider    = aws.region-worker
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
#Please note that this code expects SSH key pair to exist in default dir under
#users home directory, otherwise it will fail

#Create key-pair for logging into EC2 in us-east-1
resource "aws_key_pair" "master-key" {
  provider   = aws.region-master
  key_name   = "jenkins"
  public_key = file("~/.ssh/id_rsa.pub")
}

#Create key-pair for logging into EC2 in us-west-2
resource "aws_key_pair" "worker-key" {
  provider   = aws.region-worker
  key_name   = "jenkins"
  public_key = file("~/.ssh/id_rsa.pub")
}
#Create key-pair for logging into EC2 in us-west-2
resource "aws_key_pair" "sonar-key" {
  provider   = aws.region-worker
  key_name   = "sonar"
  public_key = file("~/.ssh/id_rsa.pub")
}
#Create key-pair for logging into EC2 in us-west-2
resource "aws_key_pair" "nexus-key" {
  provider   = aws.region-master
  key_name   = "nexus"
  public_key = file("~/.ssh/id_rsa.pub")
}

#Create and bootstrap EC2 in us-east-1
resource "aws_instance" "jenkins-master" {
  provider                    = aws.region-master
  ami                         = data.aws_ami.ubuntu_master.id
  instance_type               = var.instance-type
  key_name                    = aws_key_pair.master-key.key_name
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.jenkins-sg.id]
  subnet_id                   = aws_subnet.subnet_1.id
 
  root_block_device {
    volume_type           = "gp2"
    volume_size           = 100
    delete_on_termination = false
  }
  tags = {
    Name = "jenkins_master_tf"
  }

  depends_on = [aws_main_route_table_association.set-master-default-rt-assoc]
  #The code below is ONLY the provisioner block which needs to be
  #inserted inside the resource block for Jenkins EC2 master Terraform
  #Jenkins Master Provisioner:

  provisioner "local-exec" {
    command = <<EOF
aws --profile ${var.profile} ec2 wait instance-status-ok --region ${var.region-master} --instance-ids ${self.id}
ansible-playbook --extra-vars 'passed_in_hosts=tag_Name_${self.tags.Name}' ansible_templates/install_jenkins_master.yml
EOF
  }
}

#Create EC2 in us-west-2
resource "aws_instance" "jenkins-worker-oregon" {
  provider                    = aws.region-worker
  count                       = var.workers-count
  ami                         = data.aws_ami.ubuntu_worker.id
  instance_type               = var.instance-type
  key_name                    = aws_key_pair.worker-key.key_name
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.jenkins-sg-oregon.id]
  subnet_id                   = aws_subnet.subnet_1_oregon.id
  
  root_block_device {
    volume_type           = "gp2"
    volume_size           = 100
    delete_on_termination = false
  }
  tags = {
    Name              = join("_", ["jenkins_worker_tf", count.index + 1])
    Master_Private_IP = aws_instance.jenkins-master.private_ip
  }
  depends_on = [aws_main_route_table_association.set-worker-default-rt-assoc, aws_instance.jenkins-master]

  #The code below is ONLY the provisioner block which needs to be
  #inserted inside the resource block for Jenkins EC2 worker in Terraform
  

  provisioner "local-exec" {
    command = <<EOF
aws --profile ${var.profile} ec2 wait instance-status-ok --region ${var.region-worker} --instance-ids ${self.id}
ansible-playbook --extra-vars 'passed_in_hosts=tag_Name_${self.tags.Name} master_ip=${aws_instance.jenkins-master.private_ip}' ansible_templates/install_jenkins_worker.yml
EOF
  }
  provisioner "remote-exec" {
    when = destroy
    inline = [
      "java -jar /home/ubuntu/jenkins-cli.jar -auth @/home/ubuntu/jenkins_auth -s http://${self.tags.Master_Private_IP}:8080 delete-node ${self.private_ip}"
    ]
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("~/.ssh/id_rsa")
      host        = self.public_ip
    }
  }
}

##### NEXUS INSTANCE ######################
#Create and bootstrap EC2 in us-east-1
resource "aws_instance" "nexus" {
  provider                    = aws.region-master
  ami                         = data.aws_ami.ubuntu_master.id
  instance_type               = var.instance-type
  key_name                    = aws_key_pair.nexus-key.key_name
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.nexus-sg.id]
  subnet_id                   = aws_subnet.subnet_nexus_1.id
 
  root_block_device {
    volume_type           = "gp2"
    volume_size           = 100
    delete_on_termination = false
  }
  tags = {
    Name = "nexus_master_tf"
  }
  provisioner "local-exec" {
    command = <<EOF
aws --profile ${var.profile} ec2 wait instance-status-ok --region ${var.region-master} --instance-ids ${self.id}
ansible-playbook --extra-vars 'passed_in_hosts=${aws_instance.nexus.public_ip}' ansible_templates/install_nexus.yml
EOF
  }
}
##_____________________________SONAREQUBE INSANCE________________________________________________
#Create and bootstrap EC2 in us-east-1
resource "aws_instance" "sonar" {
  provider                    = aws.region-worker
  ami                         = data.aws_ami.ubuntu_worker.id
  instance_type               = var.instance-type
  key_name                    = aws_key_pair.sonar-key.key_name
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.sonar-sg.id]
  subnet_id                   = aws_subnet.subnet_sonar_1.id
  user_data = file("/home/ali/bootcmp/cicd-jenkins-aws-terraform/ansible_templates/sonar.sh")
  root_block_device {
    volume_type           = "gp2"
    volume_size           = 100
    delete_on_termination = false
  }
  tags = {
    Name = "sonar_master_tf"
  }
  
}
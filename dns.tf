#DNS Configuration
#Get already , publicly configured Hosted Zone on Route53 - MUST EXIST
data "aws_route53_zone" "dns" {
  provider = aws.region-master
  name     = var.dns-name
}
data "aws_route53_zone" "dns_sonar" {
  provider = aws.region-worker
  name     = var.dns-name
}

#Create record in hosted zone for ACM Certificate Domain verification
resource "aws_route53_record" "cert_validation" {
  provider = aws.region-master
  for_each = {
    for val in aws_acm_certificate.jenkins-lb-https.domain_validation_options : val.domain_name => {
      name   = val.resource_record_name
      record = val.resource_record_value
      type   = val.resource_record_type
    }
  }
  name    = each.value.name
  records = [each.value.record]
  ttl     = 60
  type    = each.value.type
  zone_id = data.aws_route53_zone.dns.zone_id
}
#Create Alias record towards  JENKINS ALB from route53
resource "aws_route53_record" "jenkins" {
  provider = aws.region-master
  zone_id  = data.aws_route53_zone.dns.zone_id
  name     = join(".", ["jenkins", data.aws_route53_zone.dns.name])
  type     = "A"
  alias {
    name                   = aws_lb.application-lb.dns_name
    zone_id                = aws_lb.application-lb.zone_id
    evaluate_target_health = true
  }
}
#______________________________Create Alias record towards NEXUS ALB from route53_____________________
#Create record in hosted zone for ACM Certificate Domain verification
resource "aws_route53_record" "cert_validation_nexus" {
  provider = aws.region-master
  for_each = {
    for val in aws_acm_certificate.nexus-lb-https.domain_validation_options : val.domain_name => {
      name   = val.resource_record_name
      record = val.resource_record_value
      type   = val.resource_record_type
    }
  }
  name    = each.value.name
  records = [each.value.record]
  ttl     = 60
  type    = each.value.type
  zone_id = data.aws_route53_zone.dns.zone_id
}
#Create Alias record towards  JENKINS ALB from route53
resource "aws_route53_record" "nexus" {
  provider = aws.region-master
  zone_id  = data.aws_route53_zone.dns.zone_id
  name     = join(".", ["nexus", data.aws_route53_zone.dns.name])
  type     = "A"
  alias {
    name                   = aws_lb.application-lb-nexus.dns_name
    zone_id                = aws_lb.application-lb-nexus.zone_id
    evaluate_target_health = true
  }
}
#______________________________Create Alias record towards SONARQUBE ALB from route53_____________________
#Create record in hosted zone for ACM Certificate Domain verification
resource "aws_route53_record" "cert_validation_sonar" {
  provider = aws.region-worker
  for_each = {
    for val in aws_acm_certificate.sonar-lb-https.domain_validation_options : val.domain_name => {
      name   = val.resource_record_name
      record = val.resource_record_value
      type   = val.resource_record_type
    }
  }
  name    = each.value.name
  records = [each.value.record]
  ttl     = 60
  type    = each.value.type
  zone_id = data.aws_route53_zone.dns_sonar.zone_id
}
#Create Alias record towards  JENKINS ALB from route53
resource "aws_route53_record" "sonar" {
  provider = aws.region-worker
  zone_id  = data.aws_route53_zone.dns_sonar.zone_id
  name     = join(".", ["sonar", data.aws_route53_zone.dns.name])
  type     = "A"
  alias {
    name                   = aws_lb.application-lb-sonar.dns_name
    zone_id                = aws_lb.application-lb-sonar.zone_id
    evaluate_target_health = true
  }
}
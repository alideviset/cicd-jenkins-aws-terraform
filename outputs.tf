output "az" {
  value = data.aws_availability_zones.azs
}

###This chunk of template can also be put inside outputs.tf for better segregation
output "Jenkins-Main-Node-Public-IP" {
  value = aws_instance.jenkins-master.public_ip
}

output "Jenkins-Worker-Public-IPs" {
  value = {
    for instance in aws_instance.jenkins-worker-oregon :
    instance.id => instance.public_ip
  }
}

#Add LB DNS name to outputs.tf
output "LB-DNS-NAME" {
  value = aws_lb.application-lb.dns_name
}

output "amiId-us-east-1" {
  value     = data.aws_ami.ubuntu_master.id
  sensitive = true
}

output "amiId-us-west-2" {
  value     = data.aws_ami.ubuntu_worker.id
  sensitive = true
}
output "Jenkins-Main-Node-Private-IP" {
  value = aws_instance.jenkins-master.private_ip
}
output "Jenkins-Worker-Private-IPs" {
  value = {
    for instance in aws_instance.jenkins-worker-oregon :
    instance.id => instance.private_ip
  }
}

output "url_jenkins" {
  value = aws_route53_record.jenkins.fqdn
}
output "url_nexus" {
  value = aws_route53_record.nexus.fqdn
}
output "url_sonar" {
  value = aws_route53_record.sonar.fqdn
}
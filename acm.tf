#ACM CONFIGURATION
#Creates ACM certificate and requests validation via DNS(Route53)
resource "aws_acm_certificate" "jenkins-lb-https" {
  provider          = aws.region-master
  domain_name       = join(".", ["jenkins", data.aws_route53_zone.dns.name])
  validation_method = "DNS"
  tags = {
    Name = "Jenkins-ACM"
  }
}
#Validates ACM issued certificate via Route53
resource "aws_acm_certificate_validation" "cert" {
  provider                = aws.region-master
  certificate_arn         = aws_acm_certificate.jenkins-lb-https.arn
  for_each                = aws_route53_record.cert_validation
  validation_record_fqdns = [aws_route53_record.cert_validation[each.key].fqdn]
}
####ACM CONFIG END
resource "aws_lb_listener" "jenkins-listener" {
  provider          = aws.region-master
  load_balancer_arn = aws_lb.application-lb.arn
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = aws_acm_certificate.jenkins-lb-https.arn
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app-lb-tg.arn
  }
}
#_______________________________ACM NEXUS CONFIGURATION __________________________________
#Creates ACM certificate and requests validation via DNS(Route53)
resource "aws_acm_certificate" "nexus-lb-https" {
  provider          = aws.region-master
  domain_name       = join(".", ["nexus", data.aws_route53_zone.dns.name])
  validation_method = "DNS"
  tags = {
    Name = "Nexus-ACM"
  }
}
#Validates ACM issued certificate via Route53
resource "aws_acm_certificate_validation" "cert-nexus" {
  provider                = aws.region-master
  certificate_arn         = aws_acm_certificate.nexus-lb-https.arn
  for_each                = aws_route53_record.cert_validation_nexus
  validation_record_fqdns = [aws_route53_record.cert_validation_nexus[each.key].fqdn]
}
####ACM CONFIG END
resource "aws_lb_listener" "nexus-listener" {
  provider          = aws.region-master
  load_balancer_arn = aws_lb.application-lb-nexus.arn
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = aws_acm_certificate.nexus-lb-https.arn
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app-lb-tg-nexus.arn
  }
}
#_______________________________ACM SONARQUBE CONFIGURATION __________________________________
#Creates ACM certificate and requests validation via DNS(Route53)
resource "aws_acm_certificate" "sonar-lb-https" {
  provider          = aws.region-worker
  domain_name       = join(".", ["sonar", data.aws_route53_zone.dns_sonar.name])
  validation_method = "DNS"
  tags = {
    Name = "Sonar-ACM"
  }
}
#Validates ACM issued certificate via Route53
resource "aws_acm_certificate_validation" "cert-sonar" {
  provider                = aws.region-worker
  certificate_arn         = aws_acm_certificate.sonar-lb-https.arn
  for_each                = aws_route53_record.cert_validation_sonar
  validation_record_fqdns = [aws_route53_record.cert_validation_sonar[each.key].fqdn]
}
####ACM CONFIG END
resource "aws_lb_listener" "sonar-listener" {
  provider          = aws.region-worker
  load_balancer_arn = aws_lb.application-lb-sonar.arn
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = aws_acm_certificate.sonar-lb-https.arn
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app-lb-tg-sonar.arn
  }
}
resource "aws_lb" "application-lb" {
  provider           = aws.region-master
  name               = "jenkins-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb-sg.id]
  subnets            = [aws_subnet.subnet_1.id, aws_subnet.subnet_2.id]
  tags = {
    Name = "Jenkins-LB"
  }
}

resource "aws_lb_target_group" "app-lb-tg" {
  provider    = aws.region-master
  name        = "app-lb-tg"
  port        = var.webserver-port
  target_type = "instance"
  vpc_id      = aws_vpc.vpc_master.id
  protocol    = "HTTP"
  health_check {
    enabled  = true
    interval = 10
    path     = "/"
    port     = var.webserver-port
    protocol = "HTTP"
    matcher  = "200-299"
  }
  tags = {
    Name = "jenkins-target-group"
  }
}

resource "aws_lb_listener" "jenkins-listener-http" {
  provider          = aws.region-master
  load_balancer_arn = aws_lb.application-lb.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}


#Create new listener on tcp/443 HTTPS
resource "aws_lb_listener" "jenkins-listener-https" {
  provider          = aws.region-master
  load_balancer_arn = aws_lb.application-lb.arn
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = aws_acm_certificate.jenkins-lb-https.arn
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app-lb-tg.arn
  }
}

resource "aws_lb_target_group_attachment" "jenkins-master-attach" {
  provider         = aws.region-master
  target_group_arn = aws_lb_target_group.app-lb-tg.arn
  target_id        = aws_instance.jenkins-master.id
  port             = var.webserver-port
}

#_________________________________NEXUS ALB_______________________
resource "aws_lb" "application-lb-nexus" {
  provider           = aws.region-master
  name               = "nexus-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb-sg-nexus.id]
  subnets            = [aws_subnet.subnet_nexus_1.id, aws_subnet.subnet_nexus_2.id]
  tags = {
    Name = "NEXUS-LB"
  }
}
resource "aws_lb_target_group" "app-lb-tg-nexus" {
  provider    = aws.region-master
  name        = "app-lb-tg-nexus"
  port        = var.nexus-webserver-port
  target_type = "instance"
  vpc_id      = aws_vpc.vpc_nexus.id
  protocol    = "HTTP"
  health_check {
    enabled  = true
    interval = 10
    path     = "/"
    port     = var.nexus-webserver-port
    protocol = "HTTP"
    matcher  = "200-299"
  }
  tags = {
    Name = "nexus-target-group"
  }
}
resource "aws_lb_listener" "nexus-listener-http" {
  provider          = aws.region-master
  load_balancer_arn = aws_lb.application-lb-nexus.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}
#Create new listener on tcp/443 HTTPS
resource "aws_lb_listener" "nexus-listener-https" {
  provider          = aws.region-master
  load_balancer_arn = aws_lb.application-lb-nexus.arn
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = aws_acm_certificate.nexus-lb-https.arn
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app-lb-tg-nexus.arn
  }
}

resource "aws_lb_target_group_attachment" "nexus-attach" {
  provider         = aws.region-master
  target_group_arn = aws_lb_target_group.app-lb-tg-nexus.arn
  target_id        = aws_instance.nexus.id
  port             = var.nexus-webserver-port
}
#_________________________________SONARQUBE ALB_______________________
resource "aws_lb" "application-lb-sonar" {
  provider           = aws.region-worker
  name               = "sonar-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb-sg-sonar.id]
  subnets            = [aws_subnet.subnet_sonar_1.id, aws_subnet.subnet_sonar_2.id]
  tags = {
    Name = "SONAR-LB"
  }
}
resource "aws_lb_target_group" "app-lb-tg-sonar" {
  provider    = aws.region-worker
  name        = "app-lb-tg-sonar"
  port        = var.sonar-webserver-port
  target_type = "instance"
  vpc_id      = aws_vpc.vpc_sonar.id
  protocol    = "HTTP"
  health_check {
    enabled  = true
    interval = 10
    path     = "/"
    port     = var.sonar-webserver-port
    protocol = "HTTP"
    matcher  = "200-299"
  }
  tags = {
    Name = "sonar-target-group"
  }
}
resource "aws_lb_listener" "sonar-listener-http" {
  provider          = aws.region-worker
  load_balancer_arn = aws_lb.application-lb-sonar.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}
#Create new listener on tcp/443 HTTPS
resource "aws_lb_listener" "sonar-listener-https" {
  provider          = aws.region-worker
  load_balancer_arn = aws_lb.application-lb-sonar.arn
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = aws_acm_certificate.sonar-lb-https.arn
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app-lb-tg-sonar.arn
  }
}

resource "aws_lb_target_group_attachment" "sonar-attach" {
  provider         = aws.region-worker
  target_group_arn = aws_lb_target_group.app-lb-tg-sonar.arn
  target_id        = aws_instance.sonar.id
  port             = var.sonar-webserver-port
}